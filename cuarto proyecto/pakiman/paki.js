
var imagenes = [];
imagenes["cauchin"] = "img/vaca.png";
imagenes["pokacho"] = "img/pollo.png";
imagenes["tocinauro"] = "img/cerdo.png";

class Pakiman{
  constructor(n, v, a){
    this.imagen = new Image();
    this.nombre = n;
    this.vida = v;
    this.ataque = a;

    this.imagen.src = imagenes[this.nombre];
  }
  hablar(){
    alert(this.nombre);
  }
  mostrar(){
    document.body.appendChild(this.imagen);
    document.write("<br/> <strong>" + this.nombre + "</strong> <br/>" );
    document.write("Vida: " + this.vida + "<br/>");
    document.write("Ataque: " + this.ataque + "<hr/>");
  }
}

var coleccion = [];
coleccion.push(new Pakiman("cauchin", 100, 30));
coleccion.push(new Pakiman("pokacho", 80, 50));
coleccion.push(new Pakiman("tocinauro", 120, 40));

for (var i of coleccion){
  i.mostrar();
  console.log(i);
}

// var imagenes = [];
// imagenes["cauchin"] = "img/vaca.png";
// imagenes["pokacho"] = "img/pollo.png";
// imagenes["tocinauro"] = "img/cerdo.png";

// class Pakiman{
//     constructor(nombre, vida, ataque){
//         this.imagen = new Image();
//         this.nombre = nombre;
//         this.vida = vida;
//         this.ataque = ataque;

//         this.imagen.src = imagenes[this.nombre];
//     }
//     hablar(){
//         alert(this.nombre);
//     }
//     mostrar(){
//         document.body.appendChild(this.imagen);
//         document.write("<br><strong>" + this.nombre + "</strong><br>");
//         document.write("Vida: " + this.vida + "<br>");
//         document.write("Ataque: " + this.ataque + "<br>");
//     }
// }

// var cauchin = new Pakiman("cauchin", 100, 30);
// var pokacho = new Pakiman("pokacho", 500, 10);
// var tocinauro = new Pakiman("tocinauro", 120, 40);
