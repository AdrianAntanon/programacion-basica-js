var numeros = 100;
var primero = "fizz", segundo = "buzz";
var nombre_completo = primero + segundo;

for(let i=1;i<=numeros;i++){
    if(esDivisible(i,3) && !esDivisible(i,5)){
        document.write(`El ${i} es ${primero}`);
    }
    else if(esDivisible(i,5) && !esDivisible(i,3)){
        document.write(`El ${i} es ${segundo}`);
    }
    else if(!esDivisible(i,3) && !esDivisible(i,5)){
        document.write(`El ${i} es ${nombre_completo}`);
    }else{
        document.write(`El ${i} es divisible tanto por 3 como por 5`)
    }
    document.write("<br>");
}

function esDivisible(num, divisor){
    if(num %  divisor ==0){
        return true;
    }else{
        return false;
    }
}
