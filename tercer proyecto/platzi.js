
var vp = document.getElementById("villaplatzi");
var papel = vp.getContext("2d");

var fondo = {
    url: "img/tile.png",
    cargaOK: false
};
var vaca = {
    url: "img/vaca.png",
    cargaOK: false
};
var cerdo = {
    url: "img/cerdo.png",
    cargaOK: false
};
var pollo = {
    url: "img/pollo.png",
    cargaOK: false
};

fondo.imagen = new Image();
fondo.imagen.src = fondo.url;
fondo.imagen.addEventListener("load", cargarFondo);

vaca.imagen = new Image();
vaca.imagen.src = vaca.url;
vaca.imagen.addEventListener("load", cargarVacas);

cerdo.imagen = new Image();
cerdo.imagen.src = cerdo.url;
cerdo.imagen.addEventListener("load", cargarCerdos);

pollo.imagen = new Image();
pollo.imagen.src = pollo.url;
pollo.imagen.addEventListener("load", cargarPollos);

function cargarFondo(){
    fondo.cargaOK = true;
    dibujar();
};
function cargarVacas(){
    vaca.cargaOK = true;
    dibujar();
};
function cargarCerdos(){
    cerdo.cargaOK = true;
    dibujar();
};
function cargarPollos(){
    pollo.cargaOK = true;
    dibujar();
};

function dibujar(){
    if(fondo.cargaOK){
        papel.drawImage(fondo.imagen, 0, 0);
    }
    if(vaca.cargaOK){
        var cantidad = aleatorio(0, 3);
        console.log(cantidad);
        for(var i=0; i<cantidad; i++){
            var x = aleatorio(0, 420);
            var y = aleatorio(0, 420);
            papel.drawImage(vaca.imagen, x, y);
        }
    }
    if(cerdo.cargaOK){
        var cantidad = aleatorio(0, 6);
        console.log(cantidad);
        for(var i=0; i<cantidad; i++){
            var x = aleatorio(0, 420);
            var y = aleatorio(0, 420);
            papel.drawImage(cerdo.imagen, x, y);
        }
    }
    if(pollo.cargaOK){
        var cantidad = aleatorio(0, 10)
        console.log(cantidad);;
        for(var i=0; i<cantidad; i++){
            var x = aleatorio(0, 420);
            var y = aleatorio(0, 420);
            papel.drawImage(pollo.imagen, x, y);
        }
    }
}


function aleatorio(min, max){
    var resultado;
    resultado = Math.floor(Math.random()*(max - min +1)) + min;
    return resultado;
}